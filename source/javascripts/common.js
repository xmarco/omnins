var API_URL = 'http://localhost:8080/baibaoxiang/api';
//var API_URL = 'http://www.wanengins.com/baibaoxiang/api';
/**
 * 获取地址栏的值
 * @param param
 * @returns {*}
 */
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return null;
}

//判断是否为微信访问
function is_weixn() {
    var ua = navigator.userAgent.toLowerCase();
    if (ua.match(/MicroMessenger/i) == "micromessenger") {
        return true;
    } else {
        return false;
    }
}

//验证邮箱 
function checkEmail(email) {
    if (email == "") {
        return false;
    }
    if (!email.match(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/)) {
        return false;
    }
    return true;
}

//验证手机号码 
function checkMobil(mobile) {
    if (mobile == "") {
        return false;
    }

    var isMob = /^1[0-9]{10}$/
    if (!isMob.test(mobile)) {
        return false;
    }
    return true;
}

/**
 * 
 * @param c_name 键
 * @param value  值
 * @param millisecond cookie有效时间毫秒值
 * @return
 */
function setCookie(c_name, value, millisecond) {
    var exdate = new Date();
    exdate.setTime(exdate.getTime() + millisecond);
    document.cookie = c_name + "=" + escape(value) + ((millisecond == null) ? "" : ";expires=" + exdate.toGMTString());
}

/**
 * 获取cookie值
 * @param c_name 键
 * @return
 */
function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1){
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return ""
}

/**
 * 清除cookie
 * @param c_name 键
 * @return
 */
function clearCookie(c_name) {
    setCookie(c_name, "", -1);
}

$(document).ready(function(){
    user_id = getCookie("user_id");
    user_name = getCookie("user_name");
    user_phone = getCookie("user_phone");
    //判断用户是否登录
    if(user_id != null && user_id != ""){
        $('.btn-login').parent().hide();
        $('.btn-signup').parent().hide();
        $('.navbar-right').append('<li id="welcom">欢迎您, <a href="/member-center/">' + user_phone+'</a></li>');
        $('.navbar-right').append('<li id="logout"><a href="javascript:logout();" class="btn-nav btn-red logout">注销</a ></li>');
    }
});

function logout(){
    clearCookie("user_id");
    clearCookie("user_name");
    clearCookie("user_phone");
    $("#welcom").hide();
    $("#logout").hide();
    $('.btn-login').parent().show();
    $('.btn-signup').parent().show();
}